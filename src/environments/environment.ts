// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB-kcnbcmqqNZ9eWOZULoiKmWwh-lxB-cI',
    authDomain: 'controle-ayla.firebaseapp.com',
    projectId: 'controle-ayla',
    storageBucket: 'controle-ayla.appspot.com',
    messagingSenderId: '885298453705',
    appId: '1:885298453705:web:2b7958ff8ce04c80324118',
    measurementId: 'G-VHC2ZXTKSG'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
