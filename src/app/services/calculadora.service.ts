import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividendo, divisor) {
    if (divisor == 0 || dividendo == 0) {
      return 'Não existe divisão por zero'
    } else {
      return dividendo / divisor; 
    }
  }
}
