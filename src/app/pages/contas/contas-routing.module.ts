import { ListaPage } from './lista/lista.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RelatorioPage } from './relatorio/relatorio.page';
import { CadastroPage } from './cadastro/cadastro.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: ListaPage},
      {path: 'receber', component: ListaPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ListaPage,
    CadastroPage,
    RelatorioPage,
  ]
})
export class ContasRoutingModule { }
