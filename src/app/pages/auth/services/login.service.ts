import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from '@firebase/auth-types';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn: Observable<User>;

  constructor(
    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController,
  ) {
    this.isLoggedIn = this.auth.authState;
   }

  
  login(user){
    this.auth.signInWithEmailAndPassword(user.email, user.pass)
    .then(() => this.nav.navigateForward('home'))
    .catch (() => this.showError());
  }

  logout() {
    this.auth.signOut()
    .then(
      () => this.nav.navigateBack('auth')
    );
  }
  
  private async showError() {
    const ctrl = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000
    });
    ctrl.present();
  }
  recoverPass(data) {
    this.auth.sendPasswordResetEmail(data.email)
    .then(
      () => this.nav.navigateBack('auth')
    )
    .catch(err => {
      console.log(err);
    });
  }
  createUser(user) {
    this.auth.createUserWithEmailAndPassword(user.email, user.pass)
    .then(credentials => console.log(credentials));
  }
}
